from rabbit_exp import rabbit
from prometheus_client import start_http_server, REGISTRY
import time
import os

if __name__ == "__main__":
    start_http_server(int(os.environ.get('PORT')))
    REGISTRY.register(rabbit.RabbitCheck(str(os.environ.get('HOST'))))
    while True: time.sleep(1)