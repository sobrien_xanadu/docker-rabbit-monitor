FROM python:2
ADD run.py /
ADD rabbit_exp/ /rabbit_exp/
RUN pip install requests prometheus_client
CMD ["python", "./run.py"]