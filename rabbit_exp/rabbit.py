import requests
from requests.auth import HTTPBasicAuth
import json
from prometheus_client import Metric
from datetime import datetime


class RabbitCheck(object):
    def __init__(self, endpoint):
        self._endpoint = endpoint


    def collect(self):
        try:
            r = requests.get(self._endpoint, auth=HTTPBasicAuth('admin', 'password1'))
            response = json.loads(r.text)
            if "running" in response[0]['status']:
                status = float(1)
            else:
                status = float(0)
            idle_since = response[0]['local_channel']['idle_since'][11:]
            time_now = datetime.now().strftime('%H:%M:%S')
            FMT = "%H:%M:%S"
            idle_time = datetime.strptime(time_now, FMT) - datetime.strptime(idle_since, FMT)
            idle_time_in_mins = idle_time.total_seconds() / 60
            rabbit_status = Metric('rabbit_queue', 'Status of Rabbit queue', 'summary')
            rabbit_status.add_sample('rabbit_queue_status', value=status, labels={"node": response[0]['node']})
            rabbit_idle_time = Metric('rabbit_queue', 'Time since queue was idle', 'summary')
            rabbit_idle_time.add_sample('rabbit_queue_idle', value=float(idle_time_in_mins),
                                        labels={"node": response[0]['node'], "queue_host": self._endpoint})
            yield rabbit_status
            yield rabbit_idle_time
        except:
            rabbit = Metric('rabbit_queue', 'Status of Rabbit queue', 'summary')
            rabbit.add_sample('rabbit_queue_status', value=float(0), labels={"queue_host": self._endpoint})
            yield rabbit